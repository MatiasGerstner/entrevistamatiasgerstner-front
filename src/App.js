import React from 'react';
import { Container } from 'semantic-ui-react';
import { BrowserRouter, Switch, Route ,   Redirect} from 'react-router-dom';
import Login from "./Components/Login/Login"
import Home from "./Components/Home/Home"
import Register from "./Components/Register/Register"
import 'semantic-ui-css/semantic.min.css'

 
const isAuthenticated = () => { 
  const token = localStorage.getItem('token');
try {
if(token){
  return true;
}
else{
  return false;
}
} catch (error) {
return false;
}
}

function PrivateRoute({ component: Component, ...rest }) {
return (
<Route
  {...rest}
  render={props =>
    isAuthenticated() ? (
      <Component {...props} />
    ) : (
      <Redirect
        to={{
          pathname: "/login",
        }}
      />
    )
  }
/>
);
}

export default () => (
  <Container>
<BrowserRouter>
<Switch>
  <Route path="/" exact component={Login}/>
  <Route path="/register" exact component={Register}/>
  <Route path="/login" exact component={Login}/>
  <PrivateRoute path="/home" exact component={Home}/>
</Switch>
</BrowserRouter>
</Container>
);