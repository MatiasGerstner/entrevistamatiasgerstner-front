import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { Button, Form, Grid, Header, Image, Message, Segment } from 'semantic-ui-react'
import Photo from "../../Assets/photo.jpeg"
import { signin } from "../../Actions/auth";
import useForm from '../../hooks/useForm';
import {Link} from "react-router-dom";


const Login = () => {
  const history = useHistory();
  const dispatch = useDispatch();
  const [userError, setUserError] = useState(false);
  const [pwdError, setPwdError] = useState(false);
  const [ formValues, handleInputChange ] = useForm({
      username:'',
      password:''
  });
  const { password ,username  } = formValues;
  const handleSubmit = (e) => {
    e.preventDefault();
    if( verification() ) {

        dispatch( signin(formValues, history) );
    }
}

const verification = () => {
    if( username === "" ) {
        setUserError(true);
        return false;
    }

    if( password === "" ) {
        setPwdError(true);
        return false;
    }

    return true;
}

  return (
    <Grid textAlign='center' style={{ height: '100vh' }} verticalAlign='middle'>
      <Grid.Column style={{ maxWidth: 450 }}>
        <Header as='h2' color='teal' textAlign='center'>
          <Image src={Photo} /> Log-in to your account
        </Header>
        <Form size='large'  onSubmit={handleSubmit}>
          <Segment stacked>
            <Form.Input 
            fluid
             icon='user' 
             iconPosition='left' 
             name="username"
             placeholder='Username'  
             value={ username }
             onChange={ handleInputChange }
             error={ userError }
             />
            <Form.Input
              fluid
              icon='lock'
              iconPosition='left'
              placeholder='Password'
              name="password"
              value={ password }
              type='password'
              onChange={ handleInputChange }
              error={ pwdError }
            />
            <Button color='teal' fluid size='large'>
              Login
            </Button>
          </Segment>
        </Form>
        <Message>
          New to us?   <Link to="/register"> Sign Up</Link>
        </Message>
      </Grid.Column>
    </Grid>
)
  }
export default Login