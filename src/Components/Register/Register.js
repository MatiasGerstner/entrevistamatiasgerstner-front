import React, { useState } from 'react';
import { useDispatch} from 'react-redux';
import { Button, Form, Grid, Header, Image, Segment, Message } from 'semantic-ui-react'
import Photo from "../../Assets/photo.jpeg"
import { signup } from "../../Actions/auth";
import useForm from '../../hooks/useForm';
import {Link} from "react-router-dom";
const Login = () => {

  const dispatch = useDispatch();
  const [userError, setUserError] = useState(false);
  const [pwdError, setPwdError] = useState(false);


  const [ formValues, handleInputChange ] = useForm({
      username:'',
      password:''
  });
  const { username, password } = formValues;


  const handleSubmit = (e) => {
    e.preventDefault();
    if( verification() ) {

        dispatch( signup(formValues) );
    }
}

const verification = () => {
    if( username === "" ) {
        setUserError(true);
        return false;
    }

    if( password === "" ) {
        setPwdError(true);
        return false;
    }

    return true;
}

  return (
    <Grid textAlign='center' style={{ height: '100vh' }} verticalAlign='middle'>
      <Grid.Column style={{ maxWidth: 450 }}>
        <Header as='h2' color='teal' textAlign='center'>
          <Image src={Photo} /> Register to your account
        </Header>
        <Form size='large'  onSubmit={handleSubmit}>
          <Segment stacked>
            <Form.Input 
            fluid
             icon='user' 
             iconPosition='left' 
             name="username"
             placeholder='Username'  
             value={ username }
             onChange={ handleInputChange }
             error={ userError }
             helperText={(userError)?'Debe ingresar un nombre de usuario!':'' }
             />
            <Form.Input
              fluid
              icon='lock'
              iconPosition='left'
              placeholder='Password'
              name="password"
              value={ password }
              type='password'
              onChange={ handleInputChange }
              error={ pwdError }
              helperText={(pwdError)?'Debe ingresar una contraseña!':''}
            />
            <Button color='teal' fluid size='large'>
              Register
            </Button>
          </Segment>
        </Form>
        <Message>
        already have an account?   <Link to="/"> Come in</Link>
        </Message>
      </Grid.Column>
    </Grid>
)
  }
export default Login