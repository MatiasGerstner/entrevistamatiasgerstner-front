import React, {useState, useEffect} from "react"
import { Form, Grid, Header, Image, Segment } from 'semantic-ui-react'
import Photo from "../../Assets/photo.jpeg"
import {getUser} from "../../Api/Index"

const Home = () => {
 
  const [username, setUsername] = useState([]);
  var dateObjectName = new Date()

  useEffect( async () => {
    try {
       const res = await getUser();
       setUsername(res.data.username.username);
    } catch (err) {
       console.log(err);
    }
}, []); 

 console.log( "hola ", setUsername)
  return (
    <Grid textAlign='center' style={{ height: '100vh' }} verticalAlign='middle'>
      <Grid.Column style={{ maxWidth: 450 }}>
        <Header as='h2' color='teal' textAlign='center'>
          <Image src={Photo} /> Bienvenido {username}
        </Header>
        <Form size='large'  >
          <Segment stacked>
          La hora es:  {(dateObjectName).toString()}
          </Segment>
        </Form>
      </Grid.Column>
    </Grid>
)
  }
export default Home