import { AUTH } from "../constants/index";
import * as api from "../Api/Index";

export const signin = (formData, router) => async (dispatch) => {
  try {
    const { data } = await api.signIn(formData);

    dispatch({ type: AUTH, data });
    
    localStorage.setItem('token', data.token);

    router.push('/Home');
  } catch (error) {
    console.log(error);
  }
};

export const signup = (formData, router) => async (dispatch) => {
  try {
    const { data } = await api.signUp(formData);

    dispatch({ type: AUTH, data });

    router.push('/Login');
  } catch (error) {
    console.log(error);
  }
};
